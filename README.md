## Description

Movie Rental API wrote in Nestjs framework

## Installation

```bash
# Install dependecies
npm install
```
create empty database

create .env file with the following variables:
```
DB_TYPE=<database type: postrgres by default>
DB_HOST=<localhost by default>
DB_PORT=<5432 by default>
DB_USERNAME=<postgres by default>
DB_PASSWORD=<postgres by default>
DB_DATABASE=<moive-rental by default>
DB_ENTITIES_ROUTE=<dist/**/*.entity.js by default>
JWT_SECRET=
JWT_REFRESH_SECRET=
JWT_ACCESS_TOKEN_EXPIRATION=<1h by default>
JWT_REFRESH_TOKEN_EXPIRATION=<24 by default>
JWT_EMAIL_CONFIRMATION_TOKEN_EXPIRATION=<72h by default>
SMTP_HOST=
SMTP_PORT=
SMTP_USERNAME=
SMTP_PASSWORD=
AWS_ACCESS_KEY=
AWS_SECRET_KEY=
AWS_BUCKET=
AWS_SIGNED_URL_EXPIRATION=
```
## RUN

## Run dev mode
npm run start:dev

## Run server
npm run start

## Documentation At
/api