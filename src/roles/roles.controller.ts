import { Controller, UseGuards, Get } from '@nestjs/common';
import { RolesGuard } from './roles.guard';
import { RevokedTokenGuard } from 'src/auth/revoked-token.guard';
import { AuthGuard } from '@nestjs/passport';
import { RolesService } from './roles.service';
import { RolesPolicy } from './roles-policy.decorator';
import { Role } from './role.entity';
import { ApiTags, ApiResponse, ApiOperation, ApiBearerAuth } from '@nestjs/swagger';

@ApiTags('Roles')
@Controller('roles')
@UseGuards(AuthGuard(), RevokedTokenGuard, RolesGuard)
export class RolesController {
  constructor(private rolesService: RolesService){}

  @Get()
  @ApiResponse({ status: 200 })
  @ApiOperation({
    summary: 'Obtain list of roles',
    description: 'Endpoint only available for user with admin role'
  })
  @ApiBearerAuth()
  @RolesPolicy('admin')
  indexRoles(): Promise<Role[]>{
    return this.rolesService.getRoles();
  }
}
