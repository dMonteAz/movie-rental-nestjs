import { SetMetadata } from '@nestjs/common';

export const RolesPolicy = (...roles: string[]) => SetMetadata('roles', roles);