import { Module } from '@nestjs/common';
import { RolesController } from './roles.controller';
import { AuthModule } from 'src/auth/auth.module';
import { RolesService } from './roles.service';

@Module({
  imports: [AuthModule],
  controllers: [RolesController],
  providers: [RolesService]
})
export class RolesModule {}
