import { Entity, Column, CreateDateColumn, UpdateDateColumn, PrimaryGeneratedColumn, BaseEntity, Unique, OneToMany } from "typeorm";
import { User } from "src/users/user.entity";
import { Exclude, Expose } from "class-transformer";

@Entity()
@Unique(['label'])
@Exclude()
export class Role extends BaseEntity {
  @PrimaryGeneratedColumn()
  @Expose()
  id: number;

  @Column()
  @Expose()
  label: string;

  @CreateDateColumn({ type: 'timestamp' })
  createdAt: Date;

  @UpdateDateColumn({ type: 'timestamp' })
  updatedAt: Date;

  @OneToMany(type => User, user => user.role, { eager: false })
  users: User[];
}