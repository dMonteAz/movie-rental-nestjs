import { Injectable } from '@nestjs/common';
import { Role } from './role.entity';

@Injectable()
export class RolesService {
  async getRoles(): Promise<Role[]>{
    return await Role.find();
  }
}
