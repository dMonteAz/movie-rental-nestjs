import { Controller, Post, Body, UsePipes, ValidationPipe, Patch, Param, ParseIntPipe, Delete, UseGuards, HttpCode, UseInterceptors, UploadedFile } from '@nestjs/common';
import { MoviesService } from './movies.service';
import { Movie } from './movie.entity';
import { CreateMovieDto } from './dto/create-movie.dto';
import { UpdateMovieDto } from './dto/update-movie.dto';
import { AuthGuard } from '@nestjs/passport';
import { RolesGuard } from 'src/roles/roles.guard';
import { RevokedTokenGuard } from 'src/auth/revoked-token.guard';
import { RolesPolicy } from 'src/roles/roles-policy.decorator';
import { UserAuthenticatedDataDto } from 'src/users/dto/user-authenticated-data.dto';
import { GetAuthenticatedUser } from 'src/users/get-authenticated-user.decorator';
import { ImagesService } from 'src/images/images.service';
import { ApiTags, ApiResponse, ApiOperation, ApiBearerAuth } from '@nestjs/swagger';

@ApiTags("Movies")
@Controller('movies')
@UseGuards(AuthGuard(), RevokedTokenGuard, RolesGuard)
export class MoviesController {
  constructor(
    private moviesService: MoviesService,
    private imagesService: ImagesService,
  ){}

  @Post()
  @ApiResponse({ status: 201 })
  @ApiOperation({
    summary: 'Add movie',
    description: 'Endpoint only available for user with admin role'
  })
  @ApiBearerAuth()
  @RolesPolicy('admin')
  @UsePipes(ValidationPipe)
  createTask(@Body() createMovieDto: CreateMovieDto): Promise<Movie> {
    return this.moviesService.createMovie(createMovieDto, this.imagesService);
  }

  @Patch(':id')
  @HttpCode(200)
  @ApiResponse({ status: 200 })
  @ApiOperation({
    summary: 'Update movie fields',
    description: 'Endpoint only available for user with admin role'
   })
  @ApiBearerAuth()
  @RolesPolicy('admin')
  @UsePipes(ValidationPipe)
  updateMovie(
    @Param('id', ParseIntPipe) id: number,
    @Body() updateMovieDto: UpdateMovieDto,
    @GetAuthenticatedUser() authUser: UserAuthenticatedDataDto,
  ): Promise<Movie> {
    return this.moviesService.updateMovie(id, updateMovieDto, authUser);
  }

  @Patch(':id/availability')
  @HttpCode(200)
  @ApiResponse({ status: 200 })
  @ApiOperation({
    summary: 'Remove/Enable movie',
    description: 'Endpoint only available for user with admin role'
  })
  @ApiBearerAuth()
  @RolesPolicy('admin')
  updateMovieAvailability(@Param('id', ParseIntPipe) id: number): Promise<Movie> {
    return this.moviesService.updateMovieAvailability(id)
  }

  @Delete(':id')
  @HttpCode(204)
  @ApiResponse({ status: 204 })
  @ApiOperation({
    summary: 'Delete movie',
    description: 'Endpoint only available for user with admin role'
  })
  @ApiBearerAuth()
  @RolesPolicy('admin')
  deleteMovie(@Param('id', ParseIntPipe) id: number): Promise<void> {
    return this.moviesService.deleteMovie(id);
  }
}
