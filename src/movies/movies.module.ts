import { Module } from '@nestjs/common';
import { MoviesController } from './movies.controller';
import { MoviesService } from './movies.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MovieRepository } from './movie.repository';
import { AuthModule } from 'src/auth/auth.module';
import { MoviesUpdateLogRepository } from 'src/movies-update-logs/movies-update-log.repository';
import { MoviesGetController } from './movies-get.controller';
import { ImagesService } from 'src/images/images.service';

@Module({
  imports: [TypeOrmModule.forFeature([MovieRepository, MoviesUpdateLogRepository]), AuthModule],
  controllers: [MoviesController, MoviesGetController],
  providers: [MoviesService, ImagesService],
})
export class MoviesModule {}
