import { Entity, BaseEntity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn, OneToMany } from "typeorm";
import { Reaction } from "src/reactions/reaction.entity";
import { OrderLine } from "src/order-lines/order-line.entity";
import { MoviesUpdateLog } from "src/movies-update-logs/movies-update-log.entity";
import { Expose, Exclude, Transform } from "class-transformer";

@Entity()
@Exclude()
export class Movie extends BaseEntity {
  @PrimaryGeneratedColumn()
  @Expose()
  id: number;

  @Column()
  @Expose()
  title: string;

  @Column()
  @Expose()
  description: string;

  @Column()
  @Expose()
  stock: number;

  @Column({ type: 'bigint' })
  @Expose()
  @Transform(price => Number(price))
  rentalPrice: number;

  @Column({ type: 'bigint' })
  @Expose()
  @Transform(price => Number(price))
  salePrice: number;

  @Column()
  @Expose({ groups: ['admin'] })
  availability: boolean;

  @CreateDateColumn({ type: 'timestamp' })
  @Expose()
  createdAt: Date;

  @UpdateDateColumn({ type: 'timestamp' })
  updatedAt: Date;

  @Column()
  imageKey: string;

  @Expose({ groups: ['create'] })
  signedUrl: string;

  @Expose()
  imageUrl: string;

  @Expose()
  @Transform(likes => Number(likes))
  likes: number;

  @OneToMany(type => Reaction, reaction => reaction.movie, { eager: false })
  @Expose()
  reactions: Reaction[];

  @OneToMany(type => OrderLine, orderLine => orderLine.movie, { eager: false })
  orderLines: OrderLine[];

  @OneToMany(type => MoviesUpdateLog, moviesUpdateLog => moviesUpdateLog.movie, { eager: false })
  moviesUpdateLogs: MoviesUpdateLog[];
}