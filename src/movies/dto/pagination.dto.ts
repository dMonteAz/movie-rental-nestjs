export class PaginationDto {
  itemCount: number;

  totalItems: number;

  itemPerPage: number;

  totalPages: number;

  currentPage: number;
}