import { Movie } from "../movie.entity";
import { PaginationDto } from "./pagination.dto";
import { Expose, Exclude } from "class-transformer";

@Exclude()
export class GetMovieResultDto {
  @Expose()
  movies: Movie[];

  @Expose()
  metadata: PaginationDto;
}