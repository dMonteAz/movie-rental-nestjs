import { Expose, Exclude } from "class-transformer";

@Exclude()
export class MovieBriefDto {
  @Expose()
  id: number;

  @Expose()
  title: string;

  @Expose()
  rentalPrice: number;
  
  @Expose()
  salePrice: number;
}