import { IsOptional, IsBoolean, IsString, IsNotEmpty, IsNumber, IsPositive } from "class-validator";
import { Transform } from "class-transformer";

export class MovieFiltersDto {
  @IsOptional()
  @IsBoolean()
  @IsNotEmpty()
  @Transform(availability => availability.toLowerCase() == 'true')
  availability: boolean;

  @IsOptional()
  @IsString()
  @IsNotEmpty()
  search: string;

  @IsOptional()
  @IsString()
  @IsNotEmpty()
  sort: string;

  @IsOptional()
  @IsPositive()
  @IsNotEmpty()
  @Transform(page => Number(page))
  page: number;

  @IsOptional()
  @IsPositive()
  @IsNotEmpty()
  @Transform(itemPerPage => Number(itemPerPage))
  itemPerPage: number;
}