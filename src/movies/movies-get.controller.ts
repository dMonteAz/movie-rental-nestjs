import { Controller, UseGuards, Get, Query, ValidationPipe, ParseIntPipe, Param, UsePipes } from "@nestjs/common";
import { RevokedTokenGuard } from "src/auth/revoked-token.guard";
import { MoviesService } from "./movies.service";
import { CommonEndpointGuard } from "src/auth/common-endpoints.guard";
import { Reflector } from "@nestjs/core";
import { AuthGuard } from '@nestjs/passport';
import { UserAuthenticatedDataDto } from "src/users/dto/user-authenticated-data.dto";
import { GetAuthenticatedUser } from "src/users/get-authenticated-user.decorator";
import { MovieFiltersDto } from "./dto/movie-filters.dto";
import { Movie } from "./movie.entity";
import { ImagesService } from "src/images/images.service";
import { ApiTags, ApiResponse, ApiOperation, ApiBearerAuth } from "@nestjs/swagger";

@ApiTags("Movies")
@Controller('movies')
@UseGuards(new CommonEndpointGuard(new (AuthGuard('jwt')), new RevokedTokenGuard(new Reflector)))
export class MoviesGetController {
  constructor(private moviesService: MoviesService, private imagesService: ImagesService){}

  @Get()
  @ApiResponse({ status: 200 })
  @ApiBearerAuth()
  @ApiOperation({ 
    summary: 'Obtain list of movies',
    description: `This endpoint can receive authentication or not for both roles: user and admin
    \`\`\`
    How to use filters:
    -search: searches movie by title
    -availability: <true|false> filters movie by availability. Only for users with admin role
    -sort: Add '-' in front of the field to sort in Desc order.
           Available fields for sorting:
            'title'
            'stock'
            'availability'
            'rentalPrice'
            'salePrice'
            'createdAt'
            'likes'
    -pagination: To get paginated result send the query param: 'itemPerPage' and optionaly the query param: 'page'
    
    Example: /movies?sort=-likes&search=harry&itemPerPage=2&page=3\`\`\``
  })
  getMovies(
    @Query(new ValidationPipe({ transform: true })) filterDto: MovieFiltersDto,
    @GetAuthenticatedUser() authUser: UserAuthenticatedDataDto,
  ){
    return this.moviesService.getMovies(filterDto, authUser, this.imagesService);
  }

  @ApiResponse({ status: 200 })
  @ApiOperation({ 
    summary: 'Obtain movie by id',
    description: `This endpoint can receive authentication or not for both roles: user and admin
    Only admin will view availability field`
  })
  @ApiBearerAuth()
  @Get(':id')
  @UsePipes(new ValidationPipe({transform: true}))
  getMovieById(
    @Param('id', ParseIntPipe) id: number,
    @GetAuthenticatedUser() authUser: UserAuthenticatedDataDto,
  ): Promise<Movie>{
    return this.moviesService.getMovieById(id, this.imagesService, authUser);
  }
}