import { EntityRepository, Repository } from "typeorm";
import { Movie } from "./movie.entity";
import { CreateMovieDto } from "./dto/create-movie.dto";
import { UpdateMovieDto } from "./dto/update-movie.dto";
import { MovieFiltersDto } from "./dto/movie-filters.dto";
import { UserAuthenticatedDataDto } from "src/users/dto/user-authenticated-data.dto";
import { GetMovieResultDto } from "./dto/get-movie-result.dto";
import { plainToClass} from "class-transformer";
import { PaginationDto } from "./dto/pagination.dto";
import { ImagesService } from "src/images/images.service";

@EntityRepository(Movie)
export class MovieRepository extends Repository<Movie> {
  private sortableFields = ['title', 'stock', 'availability', 'rentalPrice', 'salePrice', 'createdAt', 'likes'];

  async createMovie(createMovieDto: CreateMovieDto, imagesService: ImagesService): Promise<Movie> {
    const { title, description, stock, rentalPrice, salePrice, availability } = createMovieDto;

    const movie = new Movie();
    movie.title = title;
    movie.description = description;
    movie.stock = stock;
    movie.rentalPrice = rentalPrice;
    movie.salePrice = salePrice;
    movie.availability = availability;
    movie.imageKey = new Date().getTime()+`-${title}`;
    movie.signedUrl = await imagesService.uploadFile(movie.imageKey);
    await movie.save();

    return plainToClass(Movie, movie, {groups: ['create']});
  }

  async updateMovie(movie: Movie, updateMovieDto: UpdateMovieDto): Promise<Movie> {
    const { title, description, stock, rentalPrice, salePrice} = updateMovieDto;

    if(title) movie.title = title;
    if(description) movie.description = description;
    if(stock) movie.stock = stock;
    if(rentalPrice) movie.rentalPrice = rentalPrice;
    if(salePrice) movie.salePrice = salePrice;
    await movie.save();

    return movie;
  }

  async getMovieById(id: number, condition?: string, groups?: string[], imageService?: ImagesService): Promise<Movie>{
    const query = this.createQueryBuilder('movie');
    query.where({id});
    if(condition){
      query.andWhere('movie.availability = :condition', {condition});
    }
    query.loadRelationCountAndMap('movie.likes', 'movie.reactions');
    const movie = await query.getOne();
    if(imageService) movie.imageUrl = await imageService.getImageUrl(movie.imageKey);
    return plainToClass(Movie, movie, {groups});
  }

  async getMovies(filterDto: MovieFiltersDto, authUser: UserAuthenticatedDataDto, imagesService: ImagesService){
    const { sort, search, availability, page = 1, itemPerPage } = filterDto;
    const query = this.createQueryBuilder('movie').leftJoin('movie.reactions','reaction');
    let groups = [];
    
    if(itemPerPage) {
      query.skip(itemPerPage * (page-1));
      query.take(itemPerPage);
    }

    if(!(authUser?.role.label === 'admin')){
      query.andWhere('movie.availability = true');
    }else{
      if(availability) query.andWhere('movie.availability = :availability', {availability});
      groups.push('admin');
    }

    if(sort) {
      if(sort[0] === '-'){
        const field = sort.slice(1);
        if(this.sortableFields.includes(field)) query.orderBy(this.transformExceptions(field,itemPerPage), 'DESC');
      } else{
        if(this.sortableFields.includes(sort)) query.orderBy(this.transformExceptions(sort,itemPerPage), 'ASC');
      }
    } else {
      query.orderBy('title', 'ASC');
    }

    if (search) {
      query.andWhere('(movie.title LIKE :search)', { search: `%${search}%` });
    }

    query.addSelect(qb => {
      return qb.select('COUNT(*)').from('reaction','re').andWhere(`"re"."movieId"=movie.id`);
    }, 'likes');
    query.groupBy('movie.id');
    
    const totalRecords = await query.getCount()
    const {entities, raw} = await query.getRawAndEntities();
    entities.map((value, index) => { value.likes = raw[index].likes });
    await Promise.all (entities.map(async (value) => value.imageUrl = await imagesService.getImageUrl(value.imageKey)));

    const pagination = this.pagination(itemPerPage, page, totalRecords, entities.length);
    const result: GetMovieResultDto = plainToClass(GetMovieResultDto, { movies: entities, metadata: pagination }, {groups});
    return result;
  }

  private pagination(itemPerPage: number, page: number, total: number, itemCount: number): null | PaginationDto {
    if(itemPerPage) {
      return {
        itemCount: itemCount,
        totalItems: total,
        itemPerPage: itemPerPage,
        totalPages:  Math.ceil(total/itemPerPage),
        currentPage: page,
      };
    }
    return null;
  }

  private transformExceptions(field: string, pagination: number): string{
    let result;
    switch(field){
      case 'saleprice': {
        result = 'salePrice';
        break;
      }
      case 'rentalprice': {
        result = 'rentalPrice';
        break;
      }
      default: {
        result =  field
      }
    }

    if(pagination) result = "movie."+result;
    return result;
  }
}