import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { MovieRepository } from './movie.repository';
import { CreateMovieDto } from './dto/create-movie.dto';
import { Movie } from './movie.entity';
import { UpdateMovieDto } from './dto/update-movie.dto';
import { MovieLogValueDto } from 'src/movies-update-logs/dto/movie-log-value.dto';
import { plainToClass, classToPlain } from 'class-transformer';
import { UserAuthenticatedDataDto } from 'src/users/dto/user-authenticated-data.dto';
import { MoviesUpdateLogRepository } from 'src/movies-update-logs/movies-update-log.repository';
import { MovieFiltersDto } from './dto/movie-filters.dto';
import { ImagesService } from 'src/images/images.service';

@Injectable()
export class MoviesService {
  constructor(
    @InjectRepository(MovieRepository)
    private movieRepository: MovieRepository,
    @InjectRepository(MoviesUpdateLogRepository)
    private movieUpdateLogRepository: MoviesUpdateLogRepository
  ){}

  async createMovie(createMovieDto: CreateMovieDto, imagesService: ImagesService): Promise<Movie> {
    return this.movieRepository.createMovie(createMovieDto, imagesService);
  }

  async updateMovie(id: number, updateMovieDto: UpdateMovieDto, authUser: UserAuthenticatedDataDto): Promise<Movie> {
    let movie = await this.movieRepository.getMovieById(id);
    const oldValues: MovieLogValueDto = plainToClass(MovieLogValueDto, {...movie});
    movie = await this.movieRepository.updateMovie(movie, updateMovieDto);
    const newValues: MovieLogValueDto = plainToClass(MovieLogValueDto, {...movie});
    await this.movieUpdateLogRepository.createLog(oldValues, newValues, movie, authUser.id);
    return movie;
  }

  async getMovieById(id: number, imagesService: ImagesService, authUser?: UserAuthenticatedDataDto): Promise<Movie> {
    const groups = [];
    let conditon; 
    if(authUser){
      if(authUser.role.label === 'admin') {
        groups.push('admin')
      }else{
        conditon = true;
      }
    }else{
      conditon = true;
    }
    const found = await this.movieRepository.getMovieById(id, conditon, groups, imagesService);
    if (!found){
      throw new NotFoundException(`Movie with id: '${id}' not found`);
    }
    return found;
  }

  async updateMovieAvailability(id: number): Promise<Movie> {
    const movie = await this.movieRepository.getMovieById(id);
    movie.availability = !movie.availability;
    await movie.save();

    return movie;
  }

  async deleteMovie(id: number): Promise<void>{
    const result = await this.movieRepository.delete({ id });

    if (result.affected === 0) {
      throw new NotFoundException(`Movie with id: '${id}' not found`);
    }
  }

  async getMovies(filterDto: MovieFiltersDto, authUser: UserAuthenticatedDataDto, imagesService: ImagesService){
    return await this.movieRepository.getMovies(filterDto, authUser, imagesService);
  }
}
