import { Injectable, BadRequestException, NotFoundException } from '@nestjs/common';
import { User } from './user.entity';
import { Role } from 'src/roles/role.entity';
import { plainToClass } from 'class-transformer';

@Injectable()
export class UsersService {
  async updateRole(userId: number, roleId: number, authUserId: number): Promise<User> {
    if(userId === authUserId){
      throw new BadRequestException('You cannot change your own role');
    }

    const user: User = await User.findOne({id: userId});

    if(!user) {
      throw new NotFoundException(`User with id: ${userId} not found`);
    }

    const role = await Role.findOne({id: roleId});

    if(!role) {
      throw new NotFoundException(`Role with id: ${roleId} not found`);
    }
    user.role = role;
    await user.save();

    return plainToClass(User, user);
  }
}
