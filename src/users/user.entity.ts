import { Entity, BaseEntity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn, Unique, ManyToOne, OneToMany, Like } from "typeorm";
import { Role } from "src/roles/role.entity";
import * as bcrypt from "bcryptjs";
import { OauthToken } from "src/auth/oauth-token.entity";
import { Reaction } from "src/reactions/reaction.entity";
import { Order } from "src/orders/order.entity";
import { MoviesUpdateLog } from "src/movies-update-logs/movies-update-log.entity";
import { Exclude, Expose } from "class-transformer";

@Entity()
@Exclude()
@Unique(['email'])
export class User extends BaseEntity {
  @PrimaryGeneratedColumn()
  @Expose()
  id: number;

  @Column()
  @Expose()
  email: string;

  @Column()
  @Expose()
  confirmed_email: boolean;

  @Column()
  password: string;

  @Column()
  salt: string;

  @CreateDateColumn({ type: 'timestamp' })
  @Expose()
  createdAt: Date;

  @UpdateDateColumn({ type: 'timestamp' })
  updatedAt: Date;

  @ManyToOne(type => Role, role => role.users, { eager: true })
  @Expose()
  role: Role;

  @OneToMany(type => OauthToken, oauthToken => oauthToken.user, { eager: false })
  oauthTokens: OauthToken[];

  @OneToMany(type => Reaction, reaction => reaction.user, { eager: false })
  reactions: Reaction[];

  @OneToMany(type => Order, order => order.user, { eager: false })
  orders: Order[];

  @OneToMany(type => MoviesUpdateLog, moviesUpdateLog => moviesUpdateLog.user, { eager: false })
  moviesUpdateLogs: MoviesUpdateLog[];

  async validatePassword(password: string): Promise<boolean> {
    const hash = await bcrypt.hash(password, this.salt)
    return hash == this.password
  }
}