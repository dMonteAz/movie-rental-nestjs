import { createParamDecorator, ExecutionContext } from "@nestjs/common";
import { UserAuthenticatedDataDto } from "src/users/dto/user-authenticated-data.dto";


export const GetAuthenticatedUser = createParamDecorator((data, ctx: ExecutionContext): UserAuthenticatedDataDto => {
  const req = ctx.switchToHttp().getRequest();
  return req.user
});