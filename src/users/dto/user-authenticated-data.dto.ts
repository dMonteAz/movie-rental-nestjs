import { Exclude, Expose } from "class-transformer";
import { Role } from "src/roles/role.entity";

@Exclude()
export class UserAuthenticatedDataDto {
  @Expose()
  id: number;

  @Expose()
  email: string;

  @Expose()
  role: Role;
}