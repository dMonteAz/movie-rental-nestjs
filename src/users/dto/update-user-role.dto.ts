import { IsNotEmpty, IsNumber, IsPositive } from "class-validator";
import { ApiProperty } from "@nestjs/swagger";

export class UpdateUserRoleDto {
  @ApiProperty()
  @IsNotEmpty()
  @IsPositive()
  roleId: number;
}