import { EntityRepository, Repository } from "typeorm";
import { User } from "./user.entity";
import { AuthCredentialsDto } from "src/auth/dto/auth-credentials.dto";
import * as bcrypt from "bcryptjs";
import { ConflictException, InternalServerErrorException } from "@nestjs/common";
import { Role } from "src/roles/role.entity";

@EntityRepository(User)
export class UserRepository extends Repository<User> {
  async  signUp(authCredentialsDto: AuthCredentialsDto): Promise<User> {
    const { email, password } = authCredentialsDto;

    const user = this.create();
    user.email = email;
    user.salt = await bcrypt.genSalt();
    user.confirmed_email = false;
    user.password = await this.hashPassword(password, user.salt);
    const defaultRole = await Role.findOne({ where: { label: 'operator' } });
    user.role = defaultRole;
    try {
      await user.save();
    }catch (err) {
      if (err.code === '23505') {
        throw new ConflictException('Username already exist');
      } else {
        throw new InternalServerErrorException();
      }
    }
    return user;
  }
  
  private async hashPassword(password: string, salt: string) {
    return bcrypt.hash(password, salt);
  }

  async validateUserPassword(authCredentialsDto: AuthCredentialsDto): Promise<User> {
    const { email, password } = authCredentialsDto;
    const user = await this.findOne({ email });

    if (user && await user.validatePassword(password)) {
      return user;
    } else {
      return null;
    }
  }
}