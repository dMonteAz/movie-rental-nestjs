import { Controller, UseGuards, Patch, Param, ParseIntPipe, ValidationPipe, UsePipes, HttpCode, Body } from '@nestjs/common';
import { RolesGuard } from 'src/roles/roles.guard';
import { RevokedTokenGuard } from 'src/auth/revoked-token.guard';
import { AuthGuard } from '@nestjs/passport';
import { RolesPolicy } from 'src/roles/roles-policy.decorator';
import { UpdateUserRoleDto } from './dto/update-user-role.dto';
import { User } from './user.entity';
import { UsersService } from './users.service';
import { UserAuthenticatedDataDto } from './dto/user-authenticated-data.dto';
import { GetAuthenticatedUser } from './get-authenticated-user.decorator';
import { ApiResponse, ApiOperation, ApiTags, ApiBearerAuth } from '@nestjs/swagger';

@ApiTags('Users')
@Controller('users')
@UseGuards(AuthGuard(), RevokedTokenGuard, RolesGuard)
export class UserController {
  constructor(private usersService: UsersService){}

  @Patch('/:id/role')
  @ApiResponse({ status: 200 })
  @ApiOperation({
    summary: 'Update user role',
    description: 'Endpoint only available for user with admin role'
  })
  @ApiBearerAuth()
  @RolesPolicy('admin')
  @HttpCode(200)
  @UsePipes(ValidationPipe)
  updateUserRole(
    @Param('id', ParseIntPipe) id: number,
    @Body() updateData: UpdateUserRoleDto,
    @GetAuthenticatedUser() authUser: UserAuthenticatedDataDto,
  ): Promise<User>{
    return this.usersService.updateRole(id, updateData.roleId, authUser.id);
  }
}
