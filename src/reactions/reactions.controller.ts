import { Controller, Post, Body, UseGuards, ParseIntPipe } from '@nestjs/common';
import { ReactionsService } from './reactions.service';
import { Reaction } from './reaction.entity';
import { RolesGuard } from 'src/roles/roles.guard';
import { RevokedTokenGuard } from 'src/auth/revoked-token.guard';
import { AuthGuard } from '@nestjs/passport';
import { RolesPolicy } from 'src/roles/roles-policy.decorator';
import { GetAuthenticatedUser } from 'src/users/get-authenticated-user.decorator';
import { UserAuthenticatedDataDto } from 'src/users/dto/user-authenticated-data.dto';
import { ApiTags, ApiResponse, ApiOperation, ApiBearerAuth } from '@nestjs/swagger';

@ApiTags('Rections')
@Controller('reactions')
@UseGuards(AuthGuard(), RevokedTokenGuard, RolesGuard)
export class ReactionsController {
  constructor(private reactionsService: ReactionsService){}

  @Post()
  @ApiResponse({ status: 201 })
  @ApiOperation({ summary: 'Like Movie' })
  @ApiBearerAuth()
  @RolesPolicy('admin', 'operator')
  createReaction(@Body('movieId') movieId: number, @GetAuthenticatedUser() authUser: UserAuthenticatedDataDto): Promise<void> {
    return this.reactionsService.createReaction(movieId, authUser);
  }
}
