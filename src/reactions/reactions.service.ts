import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ReactionRepository } from './reaction.repository';
import { Reaction } from './reaction.entity';
import { AuthSigninCredentialsDto } from 'src/auth/dto/auth-signin-credentials.dto';
import { UserAuthenticatedDataDto } from 'src/users/dto/user-authenticated-data.dto';

@Injectable()
export class ReactionsService {
  constructor(
    @InjectRepository(ReactionRepository)
    private reactionRepository: ReactionRepository
  ){}

  async createReaction(movieId: number, authenticatedUser: UserAuthenticatedDataDto): Promise<void> {
    return this.reactionRepository.createReaction(movieId, authenticatedUser);
  }
}
