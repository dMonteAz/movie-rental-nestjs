import { EntityRepository, Repository } from "typeorm";
import { Reaction } from "./reaction.entity";
import { AuthSigninCredentialsDto } from "src/auth/dto/auth-signin-credentials.dto";
import { UserAuthenticatedDataDto } from "src/users/dto/user-authenticated-data.dto";
import { Movie } from "src/movies/movie.entity";
import { User } from "src/users/user.entity";
import { BadRequestException, InternalServerErrorException } from "@nestjs/common";

@EntityRepository(Reaction)
export class ReactionRepository extends Repository<Reaction> {
  async createReaction(movieId: number, authenticatedUser: UserAuthenticatedDataDto): Promise<void>{
    const reaction = new Reaction;
    const movie = await Movie.findOne({ id: movieId});
    if(!movie) {
      throw new BadRequestException(`Movie with id: ${movieId} not found`)
    }
    const user = await User.findOne({id: authenticatedUser.id });
    reaction.user = user;
    reaction.movie = movie;
    try {
      await reaction.save();
    } catch(err) {
      if (err.code === '23505') {
        throw new BadRequestException(`You have already liked this movie`);
      } else{ 
        throw new InternalServerErrorException('Save to database failed');
      }
    }
  }
}