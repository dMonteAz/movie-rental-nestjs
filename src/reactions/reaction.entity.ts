import { Entity, Unique, BaseEntity, PrimaryGeneratedColumn, UpdateDateColumn, CreateDateColumn, ManyToOne } from "typeorm";
import { User } from "src/users/user.entity";
import { Movie } from "src/movies/movie.entity";

@Entity()
@Unique(['user', 'movie'])
export class Reaction extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @CreateDateColumn({ type: 'timestamp' })
  createdAt: Date;

  @UpdateDateColumn({ type: 'timestamp' })
  updatedAt: Date;

  @ManyToOne(type => User, user => user.reactions, { eager: false })
  user: User;

  @ManyToOne(type => Movie, movie => movie.reactions, { eager: false })
  movie: Movie;
}