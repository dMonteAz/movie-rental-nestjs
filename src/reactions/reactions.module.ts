import { Module } from '@nestjs/common';
import { ReactionsController } from './reactions.controller';
import { ReactionsService } from './reactions.service';
import { AuthModule } from 'src/auth/auth.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ReactionRepository } from './reaction.repository';

@Module({
  imports: [TypeOrmModule.forFeature([ReactionRepository]),AuthModule],
  controllers: [ReactionsController],
  providers: [ReactionsService]
})
export class ReactionsModule {}
