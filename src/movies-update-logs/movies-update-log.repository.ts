import { EntityRepository, Repository } from "typeorm";
import { MoviesUpdateLog } from "./movies-update-log.entity";
import { MovieLogValueDto } from "./dto/movie-log-value.dto";
import { Movie } from "src/movies/movie.entity";
import { classToPlain } from "class-transformer";
import { User } from "src/users/user.entity";

@EntityRepository(MoviesUpdateLog)
export class MoviesUpdateLogRepository extends Repository<MoviesUpdateLog> {
  async createLog(oldValues: MovieLogValueDto, newValues: MovieLogValueDto, movie: Movie, userId: number){
    const log = new MoviesUpdateLog();
    log.oldValues = JSON.stringify(classToPlain(oldValues));
    log.newValues = JSON.stringify(classToPlain(newValues));
    log.movie = movie;
    log.user = await User.findOne({id: userId});
    await log.save();
  }
}