import { Exclude, Expose } from "class-transformer";

@Exclude()
export class MovieLogValueDto {
  @Expose()
  title: string;

  @Expose()
  salePrice: number;

  @Expose()
  rentalPrice: number;
}