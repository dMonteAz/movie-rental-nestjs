import { Entity, BaseEntity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn, ManyToOne } from "typeorm";
import { User } from "src/users/user.entity";
import { Movie } from "src/movies/movie.entity";

@Entity()
export class MoviesUpdateLog extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  oldValues: string;

  @Column()
  newValues: string;

  @CreateDateColumn({ type: 'timestamp' })
  createdAt: Date;

  @UpdateDateColumn({ type: 'timestamp' })
  updatedAt: Date;

  @ManyToOne(type => User, user => user.moviesUpdateLogs, { eager: false })
  user: User;

  @ManyToOne(type => Movie, movie => movie.moviesUpdateLogs, {eager: false })
  movie: Movie;
}