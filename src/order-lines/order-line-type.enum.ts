export enum OrderLineType {
  SALE = 'sale',
  RENT = 'rent'
}