import { Injectable, NotFoundException } from '@nestjs/common';
import { AddMovieToCartDto } from './dto/addMovieToCart.dto';
import { UserAuthenticatedDataDto } from 'src/users/dto/user-authenticated-data.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { OrderRepository } from 'src/orders/order.repository';
import { Order } from 'src/orders/order.entity';
import { OrderLine } from './order-line.entity';
import { OrderLineRepository } from './order-line.repository';
import { AddedMovieDto } from './dto/addedMovie.dto';
import { plainToClass } from 'class-transformer';
import { OrderLineStatus } from './order-line-status.enum';

@Injectable()
export class OrderLinesService {
  constructor(
    @InjectRepository(OrderRepository)
    private orderRepository: OrderRepository,
    @InjectRepository(OrderLine)
    private orderLineRepository: OrderLineRepository
  ){}

  async addOrderLineToCart(movieToAdd: AddMovieToCartDto, authUser: UserAuthenticatedDataDto): Promise<AddedMovieDto>{
    let currentCart = await Order.findOne({ where: { user: authUser.id, status: 'pending' }});
    if(!currentCart) {
      currentCart = await this.orderRepository.createOrder(authUser.id);
    }

    const orderLine = await this.orderLineRepository.crateOrderLine(movieToAdd, currentCart);
    const addedMovie: AddedMovieDto = plainToClass(AddedMovieDto, {...orderLine, orderId: currentCart.id})
    return addedMovie;
  }

  async deleteOrderLineFromCart(id: number, authUser: UserAuthenticatedDataDto): Promise<void> {
    const currentCart: Order = await Order.findOne({ where: { user: authUser.id, status: 'pending' }});

    if(!currentCart){
      throw new NotFoundException(`OrderLine with id: '${id}' not found in your current cart`)
    }
    console.log(currentCart);
    const result = await this.orderLineRepository.delete({ id, order: currentCart });

    if (result.affected === 0) {
      throw new NotFoundException(`OrderLine with id: '${id}' not found in your current cart`);
    }
  }

  async checkRentedMovie(id: number): Promise<any>{
    const orderLine = await OrderLine.findOne({id, status: OrderLineStatus.RENTED});

    if(!orderLine){
      throw new NotFoundException(`Movie with order line id: ${id} is not found`)
    }

    const currentDate = new Date();
    const rentedDate = new Date(orderLine.rentedAt);
    const daysPassed = this.calculateDateDiff(currentDate, rentedDate);
    const extraFee = this.calculateOverchargeFee(daysPassed, orderLine.daysRented, orderLine.movie.salePrice); 
    return { ...orderLine, extraFee: extraFee };
  }

  async returnRentedMovie(id): Promise<OrderLine> {
    const orderLine = await OrderLine.findOne({id, status: OrderLineStatus.RENTED});

    if(!orderLine){
      throw new NotFoundException(`Movie with order line id: ${id} is not found`)
    }

    orderLine.status = OrderLineStatus.COMPLETED;
    orderLine.save();
    return orderLine;
  }

  private calculateDateDiff(date1: Date, date2: Date): number {
    const currentDate = date1.valueOf();
    const rentedDate = date2.valueOf();
    var one_day=1000*60*60*24;
    const result = Math.round((currentDate - rentedDate)/one_day);
    return result;
  }

  private calculateOverchargeFee(daysPassed: number, daysRented: number, salePrice: number): number {
    const daysDifference = daysPassed - daysRented;
    if(daysDifference > 0){
      console.log(daysDifference);
      return ((salePrice/2) * daysDifference); 
    }
    return 0;
  }
}