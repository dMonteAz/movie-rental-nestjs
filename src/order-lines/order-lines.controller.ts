import { Controller, UseGuards, Post, Body, ValidationPipe, Delete, ParseIntPipe, Param, HttpCode, Get } from '@nestjs/common';
import { RolesGuard } from 'src/roles/roles.guard';
import { RevokedTokenGuard } from 'src/auth/revoked-token.guard';
import { AuthGuard } from '@nestjs/passport';
import { RolesPolicy } from 'src/roles/roles-policy.decorator';
import { GetAuthenticatedUser } from 'src/users/get-authenticated-user.decorator';
import { UserAuthenticatedDataDto } from 'src/users/dto/user-authenticated-data.dto';
import { AddMovieToCartDto } from './dto/addMovieToCart.dto';
import { OrderLinesService } from './order-lines.service';
import { AddedMovieDto } from './dto/addedMovie.dto';
import { OrderLine } from './order-line.entity';
import { ApiTags, ApiResponse, ApiOperation, ApiBearerAuth } from '@nestjs/swagger';

@ApiTags('Order Lines')
@Controller('order-lines')
@UseGuards(AuthGuard(), RevokedTokenGuard, RolesGuard)
export class OrderLinesController {
  constructor(private orderLinesService: OrderLinesService){}

  @Post()
  @ApiResponse({ status: 201 })
  @ApiOperation({ summary: "Add movie to logged in user's cart" })
  @ApiBearerAuth()
  @RolesPolicy('admin', 'operator')
  addMovieToCart(@Body(ValidationPipe) movieToAdd: AddMovieToCartDto, @GetAuthenticatedUser() authUser: UserAuthenticatedDataDto): Promise<AddedMovieDto>{
    return this.orderLinesService.addOrderLineToCart(movieToAdd, authUser);
  }

  @Delete(':id')
  @ApiResponse({ status: 204 })
  @ApiOperation({ summary: "Remove movie from logged in user's cart" })
  @ApiBearerAuth()
  @HttpCode(204)
  @RolesPolicy('admin', 'operator')
  removeMovieFromCart(@Param('id', ParseIntPipe) id: number, @GetAuthenticatedUser() authUser: UserAuthenticatedDataDto): Promise<void>{
    return this.orderLinesService.deleteOrderLineFromCart(id, authUser);
  }

  @Get(':id/check-rent')
  @ApiResponse({ status: 200 })
  @ApiOperation({ 
    summary: 'Check rented movie details',
    description: 'Endpoint only available for user with admin role'
  })
  @ApiBearerAuth()
  @RolesPolicy('admin')
  checkRentedMovie(
    @Param('id', ParseIntPipe) id: number
  ): Promise<any>{
    return this.orderLinesService.checkRentedMovie(id);
  }

  @Post(':id/return')
  @ApiResponse({ status: 200 })
  @ApiOperation({
    summary: 'Return rented movie',
    description: 'Endpoint only available for user with admin role'
  })
  @ApiBearerAuth()
  @HttpCode(200)
  @RolesPolicy('admin')
  returnRentedMovie(
    @Param('id', ParseIntPipe) id: number
  ): Promise<OrderLine>{
    return this.orderLinesService.returnRentedMovie(id);
  }
}
