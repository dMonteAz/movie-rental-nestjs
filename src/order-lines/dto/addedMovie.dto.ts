import { Exclude, Expose } from "class-transformer";
import { OrderLineStatus } from "../order-line-status.enum";
import { OrderLineType } from "../order-line-type.enum";

@Exclude()
export class AddedMovieDto {
  @Expose()
  id: number;

  @Expose()
  price: number;

  @Expose()
  quantity: number;

  @Expose()
  type: OrderLineType;

  @Expose()
  status: OrderLineStatus;

  @Expose()
  daysRented: number;

  @Expose()
  orderId: Number;
}