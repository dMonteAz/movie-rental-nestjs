import { Exclude, Expose, Type } from "class-transformer";
import { OrderLineStatus } from "../order-line-status.enum";
import { OrderLineType } from "../order-line-type.enum";
import { Movie } from "src/movies/movie.entity";
import { MovieBriefDto } from "src/movies/dto/movie-brief.dto"

@Exclude()
export class OrderLineCartDto {
  @Expose()
  id: number;

  @Expose()
  price: number;

  @Expose()
  quantity: number;

  @Expose()
  status: OrderLineStatus;

  @Expose()
  type: OrderLineType;

  @Expose()
  rentedAt: Date;

  @Expose()
  daysRented: number;

  @Expose()
  returnedAt: Date;

  @Expose()
  @Type(() => MovieBriefDto)
  movie: MovieBriefDto;
}