import { IsNotEmpty, ValidateIf, IsPositive, IsNumber, IsIn } from "class-validator";
import { OrderLineType } from "../order-line-type.enum";
import { ApiProperty } from "@nestjs/swagger";

export class AddMovieToCartDto {
  @ApiProperty()
  @IsNotEmpty()
  @IsNumber()
  movieId: number;

  @ApiProperty()
  @IsNotEmpty()
  @IsPositive()
  quantity: number;

  @ApiProperty()
  @IsNotEmpty()
  @IsIn([OrderLineType.RENT, OrderLineType.SALE])
  type: OrderLineType;

  @ApiProperty()
  @ValidateIf(object => object.type === "rent")
  @IsNotEmpty()
  @IsPositive()
  daysRented: number;
}