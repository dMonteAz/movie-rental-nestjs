import { OrderLine } from "./order-line.entity";
import { EntityRepository, Repository } from "typeorm";
import { Order } from "src/orders/order.entity";
import { Movie } from "src/movies/movie.entity";
import { AddMovieToCartDto } from "./dto/addMovieToCart.dto";
import { NotFoundException, ConflictException } from "@nestjs/common";
import { OrderLineStatus } from "./order-line-status.enum";

@EntityRepository(OrderLine)
export class OrderLineRepository extends Repository<OrderLine> {
  async crateOrderLine(movieData: AddMovieToCartDto,  order: Order): Promise<OrderLine> {
    const movie = await Movie.findOne({ id: movieData.movieId })
    if(!movie) {
      throw new NotFoundException(`Movie with id: ${movieData.movieId} not found`);
    }
    
    const orderLine = new OrderLine();
    if(movieData.type === 'rent') {
      orderLine.price = movie.rentalPrice;
      orderLine.daysRented = movieData.daysRented;
    }else{
      orderLine.price = movie.salePrice;
    }
    orderLine.status = OrderLineStatus.PENDING;
    if(movie.stock < movieData.quantity) throw new ConflictException(`There is not enough quantity of movie with id: ${movie.id} to complete your request`)
    orderLine.quantity = movieData.quantity;
    orderLine.type = movieData.type;
    orderLine.movie = movie;
    orderLine.order = order;
    await orderLine.save();
    return orderLine;
  }
}