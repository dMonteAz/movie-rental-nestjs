export enum OrderLineStatus {
  PENDING = 'pending',
  RENTED = 'rented',
  COMPLETED = 'completed'
}