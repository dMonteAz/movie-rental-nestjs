import { Entity, BaseEntity, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn, Column, ManyToOne } from "typeorm";
import { OrderLineStatus } from './order-line-status.enum'
import { Order } from "src/orders/order.entity";
import { Movie } from "src/movies/movie.entity";
import { OrderLineType } from "./order-line-type.enum";

@Entity()
export class OrderLine extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'bigint' })
  price: number;

  @Column()
  quantity: number;

  @Column()
  status: OrderLineStatus;

  @Column()
  type: OrderLineType;

  @Column({ nullable: true })
  rentedAt: Date;

  @Column({ nullable: true })
  daysRented: number;

  @Column({ nullable: true })
  returnedAt: Date;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @ManyToOne(type => Order, order => order.orderLines, { eager: false })
  order: Order;

  @ManyToOne(type => Movie, movie => movie.orderLines, { eager: true })
  movie: Movie;
}