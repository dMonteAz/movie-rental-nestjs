import { Module } from '@nestjs/common';
import { OrderLinesController } from './order-lines.controller';
import { OrderLinesService } from './order-lines.service';
import { AuthModule } from 'src/auth/auth.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { OrderRepository } from 'src/orders/order.repository';
import { OrderLineRepository } from './order-line.repository';

@Module({
  imports: [TypeOrmModule.forFeature([OrderRepository, OrderLineRepository]), AuthModule],
  controllers: [OrderLinesController],
  providers: [OrderLinesService]
})
export class OrderLinesModule {}
