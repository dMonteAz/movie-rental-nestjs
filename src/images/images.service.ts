import * as AWS from 'aws-sdk';

export class ImagesService {
  private s3 = new AWS.S3({
    accessKeyId: process.env.AWS_ACCESS_KEY,
    secretAccessKey: process.env.AWS_SECRET_KEY,
  });
  
  async uploadFile(key: string): Promise<string> {
    const params = {
      Bucket: process.env.AWS_BUCKET,
      Key: key,
      Expires: parseInt(process.env.AWS_SIGNED_URL_EXPIRATION) || 120
    };
    return this.s3.getSignedUrl('putObject', params);
  }

  async getImageUrl(key: string): Promise<string> {
    const params = {
      Bucket: process.env.AWS_BUCKET,
      Key: key
    };
    return this.s3.getSignedUrl('getObject', params);
  }
}