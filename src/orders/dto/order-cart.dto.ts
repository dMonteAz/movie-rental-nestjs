import { Exclude, Expose, Type } from "class-transformer";
import { OrderStatus } from "../order-status.enum";
import { OrderLineCartDto } from "src/order-lines/dto/order-line-cart.dto";

@Exclude()
export class OrderCartDto {
  @Expose()
  id: number;

  @Expose()
  transactionTotal: number;

  @Expose()
  status: OrderStatus;

  @Expose()
  @Type(() => OrderLineCartDto)
  orderLines: OrderLineCartDto[];
}