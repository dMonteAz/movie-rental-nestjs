import { Injectable, BadRequestException, ConflictException, NotFoundException } from '@nestjs/common';
import { UserAuthenticatedDataDto } from 'src/users/dto/user-authenticated-data.dto';
import { Order } from './order.entity';
import { plainToClass } from 'class-transformer';
import { OrderCartDto } from './dto/order-cart.dto';
import { OrderLineType } from 'src/order-lines/order-line-type.enum';
import { OrderLineStatus } from 'src/order-lines/order-line-status.enum';
import { OrderStatus } from './order-status.enum';
import { Connection } from 'typeorm';
import { Movie } from 'src/movies/movie.entity';
import { OrderLine } from 'src/order-lines/order-line.entity';

@Injectable()
export class OrdersService {
  constructor(private connection: Connection){}

  async checkoutCurrentUserOrder(authUser: UserAuthenticatedDataDto): Promise<OrderCartDto>{
    const currentCart = await this.findCurrentCart(authUser.id);
    let transactionTotal = 0;
    for(let orderLine of currentCart.orderLines){
      transactionTotal += (orderLine.quantity * orderLine.price);
    }
    currentCart.transactionTotal = transactionTotal;
    return plainToClass(OrderCartDto, {...currentCart});
  }

  async completeCheckout(authUser: UserAuthenticatedDataDto) {
    const currentCart = await this.findCurrentCart(authUser.id);
    let transactionTotal = 0;
    const queryRunner = this.connection.createQueryRunner();

    await queryRunner.connect();
    await queryRunner.startTransaction();
    try{
      for(let orderLine of currentCart.orderLines){
        transactionTotal += (orderLine.quantity * orderLine.price);
        if(orderLine.type === OrderLineType.RENT){
          orderLine.status = OrderLineStatus.RENTED;
          orderLine.rentedAt = new Date(Date.now());
        }else{
          orderLine.status = OrderLineStatus.COMPLETED
        }
        orderLine.movie.stock -= orderLine.quantity;
        if(orderLine.movie.stock < 0) throw new ConflictException(`There is not enough quantity of movie with id: ${orderLine.movie.id} to complete your request`);
        await queryRunner.manager.save(Movie, orderLine.movie);
        await queryRunner.manager.save(OrderLine, orderLine);
      }
      currentCart.transactionTotal = transactionTotal;
      currentCart.status = OrderStatus.COMPLETED;
      currentCart.completedAt = new Date(Date.now());
      await queryRunner.manager.save(Order, currentCart);
      await queryRunner.commitTransaction();
    }catch(err){
      await queryRunner.rollbackTransaction();
      throw new ConflictException(err.message);
    }finally{
      await queryRunner.release();
    }
    return plainToClass(OrderCartDto, {...currentCart});
  }

  async getCartById(id: number): Promise<Order> {
    const order = await Order.findOne({ id });

    if(!order) {
      throw new NotFoundException(`Cart with id: ${id} not found`);
    }
    return plainToClass(Order, order);
  }

  private async findCurrentCart(userId: number): Promise<Order>{
    const cart = await Order.findOne({where: { user: userId, status: 'pending' }});

    if(!cart){
      throw new BadRequestException("You don't have a open car");
    }

    return cart;
  }
}
