import { Controller, UseGuards, Get, Post, HttpCode, UsePipes, ValidationPipe, ParseIntPipe, Param } from '@nestjs/common';
import { RolesGuard } from 'src/roles/roles.guard';
import { RevokedTokenGuard } from 'src/auth/revoked-token.guard';
import { AuthGuard } from '@nestjs/passport';
import { OrdersService } from './orders.service';
import { RolesPolicy } from 'src/roles/roles-policy.decorator';
import { GetAuthenticatedUser } from 'src/users/get-authenticated-user.decorator';
import { UserAuthenticatedDataDto } from 'src/users/dto/user-authenticated-data.dto';
import { OrderCartDto } from './dto/order-cart.dto';
import { Order } from './order.entity';
import { ApiTags, ApiResponse, ApiOperation, ApiBearerAuth } from '@nestjs/swagger';

@ApiTags('Orders')
@Controller('orders')
@UseGuards(AuthGuard(), RevokedTokenGuard, RolesGuard)
export class OrdersController {
  constructor(private ordersService: OrdersService){}

  @Get('/my-cart')
  @ApiResponse({ status: 200 })
  @ApiOperation({ summary: "Get logged in user's open cart" })
  @ApiBearerAuth()
  @RolesPolicy('admin', 'operator')
  getCurrentCart(@GetAuthenticatedUser() authUser: UserAuthenticatedDataDto): Promise<OrderCartDto>{
    return this.ordersService.checkoutCurrentUserOrder(authUser);
  }

  @Post('/checkout')
  @ApiResponse({ status: 200 })
  @ApiOperation({ summary: "Checkout logged in user's open cart" })
  @ApiBearerAuth()
  @HttpCode(200)
  @RolesPolicy('admin', 'operator')
  completeCheckout(@GetAuthenticatedUser() authUser: UserAuthenticatedDataDto): Promise<OrderCartDto>{
    return this.ordersService.completeCheckout(authUser);
  }

  @Get(':id')
  @ApiResponse({ status: 200 })
  @ApiOperation({
    summary: 'Get cart by id',
    description: 'Endpoint only available for user with admin role'
  })
  @ApiBearerAuth()
  @RolesPolicy('admin')
  @UsePipes(ValidationPipe)
  getCart(
    @Param('id', ParseIntPipe) id: number,
  ): Promise<Order>{
    return this.ordersService.getCartById(id);
  }


}
