import { Entity, BaseEntity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn, ManyToOne, OneToMany } from "typeorm";
import { OrderStatus } from "./order-status.enum";
import { User } from "src/users/user.entity";
import { OrderLine } from "src/order-lines/order-line.entity";
import { Exclude, Expose } from "class-transformer";

@Entity()
@Exclude()
export class Order extends BaseEntity {
  @PrimaryGeneratedColumn()
  @Expose()
  id: number;

  @Column({ default: 0, type: 'bigint' })
  @Expose()
  transactionTotal: number;

  @Column({ default: 'pending' })
  @Expose()
  status: OrderStatus;

  @Column({ nullable: true})
  @Expose()
  completedAt: Date;

  @CreateDateColumn()
  @Expose()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @ManyToOne(type => User, user => user.orders, { eager: true })
  @Expose()
  user: User;

  @OneToMany(type => OrderLine, orderLine => orderLine.order, { eager: true })
  @Expose()
  orderLines: OrderLine[];
}