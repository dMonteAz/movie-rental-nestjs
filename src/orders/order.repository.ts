import { Repository, EntityRepository } from "typeorm";
import { Order } from "./order.entity";
import { User } from "src/users/user.entity";
import { NotFoundException } from "@nestjs/common";

@EntityRepository(Order)
export class OrderRepository extends Repository<Order> {
  async createOrder(userId: number): Promise<Order> {
    const order = new Order();
    const user = await User.findOne({where: { id: userId }});
    if(!user) {
      throw new NotFoundException(`User with id: ${userId} not found`);
    }
    order.user = user;
    await order.save();

    return order;
  }
}