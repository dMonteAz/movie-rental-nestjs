import { Injectable, UnauthorizedException } from "@nestjs/common";
import { PassportStrategy } from "@nestjs/passport";
import { Strategy, ExtractJwt } from 'passport-jwt';
import { InjectRepository } from "@nestjs/typeorm";
import { UserRepository } from "src/users/user.repository";
import { JwtPayload } from "./jwt-payload.interface";
import { UserAuthenticatedDataDto } from "src/users/dto/user-authenticated-data.dto";
import { plainToClass } from "class-transformer";

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(
    @InjectRepository(UserRepository)
    private userRepository: UserRepository,
  ){
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      secretOrKey: process.env.JWT_SECRET,
    });
  }

  async validate(payload: JwtPayload){
    const { email } = payload;
    const user = await this.userRepository.findOne({ email });

    if(!user) {
      throw new UnauthorizedException();
    }

    const userData: UserAuthenticatedDataDto = plainToClass(UserAuthenticatedDataDto, {...user});

    return userData;
  }
}