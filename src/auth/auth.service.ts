import { Injectable, UnauthorizedException, BadRequestException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UserRepository } from 'src/users/user.repository';
import { AuthCredentialsDto } from './dto/auth-credentials.dto';
import { JwtService } from '@nestjs/jwt';
import { JwtPayload } from './jwt-payload.interface';
import { AuthSigninCredentialsDto } from './dto/auth-signin-credentials.dto';
import { OauthToken } from './oauth-token.entity';
import { OauthTokenRepository } from './oauth-token.repository';
import { MailsService } from 'src/mails/mails.service';
import { MailTypeEnum } from 'src/mails/mail-type.enum';
import { ConfirmEmailDto } from './dto/confirm-email.dto';
import { User } from 'src/users/user.entity';

@Injectable()
export class AuthService {
  constructor(
    @InjectRepository(UserRepository)
    private userRepository: UserRepository,
    @InjectRepository(OauthTokenRepository)
    private oautTokenRepository: OauthTokenRepository,
    private jwtService: JwtService,
    private readonly mailsrService: MailsService,
  ){}

  async signUp(authCredentialsDto: AuthCredentialsDto): Promise<void> {
    const user = await this.userRepository.signUp(authCredentialsDto);
    const payload = { email: user.email }
    const confirmationToken = await this.jwtService.sign(payload, { secret: process.env.JWT_SECRET, expiresIn: (process.env.JWT_EMAIL_CONFIRMATION_TOKEN_EXPIRATION || '72h') });
    const options = { confirmationToken }
    this.mailsrService.sendEmail([user.email], MailTypeEnum.CONFIRMATION, options);
    return;
  }

  async signIn(authSigninCredentialsDto: AuthSigninCredentialsDto): Promise<{accessToken: string, refreshToken: string}> {
    const {grant_type, email, password, refresh_token } = authSigninCredentialsDto
    const authCredentialsDto: AuthCredentialsDto = { email, password }

    let new_payload: JwtPayload = undefined;
    switch(grant_type) {
      case "refresh_token": {
        try {
          this.jwtService.verify(refresh_token, { secret: process.env.JWT_REFRESH_SECRET });
          const  payload = this.jwtService.decode(refresh_token);
          const result = await OauthToken.findOne({ where: { revokedAt: null, refreshToken: refresh_token }});
          if(!result) throw new UnauthorizedException;
          result.revokedAt = new Date(Date.now());
          await result.save();
          new_payload = { id: payload['id'], email: payload['email'] }
          break;
        } catch(err) {
          throw new UnauthorizedException('Invalid Credentials');
        }
      }
      default: {
        const user = await this.userRepository.validateUserPassword(authCredentialsDto);
        if(!user) {
          throw new UnauthorizedException('Invalid Credentials');
        }
        if(!user.confirmed_email) {
          throw new UnauthorizedException("Account hasn't been validated yet, please check your email");
        }
        new_payload = { id: user.id, email: user.email }
        break;
      }
    }

    const oauthToken: OauthToken = await this.oautTokenRepository.createAccessToken(new_payload, this.jwtService);

    return { accessToken: oauthToken.token , refreshToken: oauthToken.refreshToken };
  }

  async confirmEmail(confirmEmailData: ConfirmEmailDto): Promise<void> {
    const { confirmationToken } = confirmEmailData
    try {
      this.jwtService.verify(confirmationToken, { secret: process.env.JWT_SECRET });
      const  payload = this.jwtService.decode(confirmationToken);
      const user = await User.findOne({ email: payload['email'], confirmed_email: false })
      if(!user) {
        throw new BadRequestException('Email has already been confirmed');
      }

      user.confirmed_email = true;
      user.save()
    }catch {
      throw new BadRequestException('Token has expired');
    }
  }

  async signOut(token: string): Promise<void> {
    const oauthToken = await OauthToken.findOne({ token });
    oauthToken.revokedAt = new Date(Date.now());
    oauthToken.save()
  }
}
