import { Injectable, CanActivate, ExecutionContext, UnauthorizedException } from "@nestjs/common";
import { Reflector } from "@nestjs/core";
import { OauthToken } from "./oauth-token.entity";

@Injectable()
export class RevokedTokenGuard implements CanActivate {
  constructor(private reflector:Reflector){}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const request = context.switchToHttp().getRequest();
    const access_token= request.headers['authorization'].split(' ')[1];
    const oauthToken = await OauthToken.findOne({ where: { token: access_token, revokedAt: null }})

    if(!oauthToken){
      throw new UnauthorizedException();
    }

    return true;
  }
}