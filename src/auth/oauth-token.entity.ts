import { Entity, PrimaryGeneratedColumn, BaseEntity, Column, DeleteDateColumn, CreateDateColumn, UpdateDateColumn, ManyToOne, IsNull } from "typeorm";
import { User } from "src/users/user.entity";
import { IsOptional } from "class-validator";

@Entity()
export class OauthToken extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  token: string;

  @Column()
  refreshToken: string;

  @Column({ nullable: true})
  revokedAt: Date;

  @CreateDateColumn({ type: 'timestamp' })
  createdAt: Date;

  @UpdateDateColumn({ type: 'timestamp' })
  updatedAt: Date;

  @ManyToOne(type => User, user => user.oauthTokens, { eager: true })
  user: User;
}