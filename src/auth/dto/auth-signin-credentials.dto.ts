import { IsOptional, IsEmail, IsNotEmpty, ValidateIf, IsIn } from "class-validator";
import { GrantType } from "../grant-type.enum";
import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger";

export class AuthSigninCredentialsDto {
  @ApiProperty()
  @IsNotEmpty()
  @IsIn([GrantType.PASSWORD, GrantType.REFRESH_TOKEN])
  grant_type: GrantType;

  @ApiPropertyOptional()
  @ValidateIf(object => object.grant_type === "password")
  @IsNotEmpty()
  @IsEmail()
  email: string;

  @ApiPropertyOptional()
  @ValidateIf(object => object.grant_type === "password")
  @IsNotEmpty()
  password: string;

  @ApiPropertyOptional()
  @ValidateIf(object => object.grant_type === "refresh_token")
  @IsNotEmpty()
  refresh_token: string;
}