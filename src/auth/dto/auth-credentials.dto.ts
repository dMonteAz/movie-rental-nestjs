import { IsString, MinLength, MaxLength, Matches, IsEmail, IsNotEmpty } from "class-validator";
import { ApiProperty } from "@nestjs/swagger";

export class AuthCredentialsDto {
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  @IsEmail()
  email: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  @MinLength(8)
  @Matches(/(?=.*d)(?=.*\W+)(?=.*[A-Z])(?=.*[a-z]).*$/,
    { message: 'password too weak, it should contain at least 1 letter, 1 number, 1 capital uppercase letter, 1 special symbol and be at minimun 8 characters' },
  )
  password: string;
}