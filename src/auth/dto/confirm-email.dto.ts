import { IsNotEmpty, IsString } from "class-validator";
import { ApiProperty } from "@nestjs/swagger";

export class ConfirmEmailDto {
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  confirmationToken: string;
}