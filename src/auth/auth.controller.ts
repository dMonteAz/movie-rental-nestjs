import { Controller, Post, Body, ValidationPipe, UseGuards, Headers, HttpCode } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthCredentialsDto } from './dto/auth-credentials.dto';
import { AuthSigninCredentialsDto } from './dto/auth-signin-credentials.dto';
import { ConfirmEmailDto } from './dto/confirm-email.dto';
import { RevokedTokenGuard } from './revoked-token.guard';
import { AuthGuard } from '@nestjs/passport';
import { ApiTags, ApiResponse, ApiOperation, ApiBearerAuth } from '@nestjs/swagger';

@ApiTags("Authorization")
@Controller('auth')
export class AuthController {
  constructor(
    private authService: AuthService,
  ){}

  @Post('signup')
  @ApiResponse({ status: 201 })
  @ApiOperation({ summary: 'Sign Up user' })
  signUp(@Body(ValidationPipe) authCredentialsDto: AuthCredentialsDto): Promise<void>{
    return this.authService.signUp(authCredentialsDto);
  }

  @Post('signin')
  @ApiResponse({ status: 200 })
  @ApiOperation({ summary: 'Sign In user' })
  @HttpCode(200)
  signIn(@Body(ValidationPipe) authSigninCredentialsDto: AuthSigninCredentialsDto): Promise<{accessToken: string, refreshToken: string}> {
    return this.authService.signIn(authSigninCredentialsDto);
  }

  @Post('confirm-email')
  @ApiResponse({ status: 200 })
  @ApiOperation({ summary: 'Confirm user email' })
  @HttpCode(200)
  confirmEmail(@Body(ValidationPipe) confirmEmailData: ConfirmEmailDto): Promise<void> {
    return this.authService.confirmEmail(confirmEmailData);
  }

  @Post('signout')
  @HttpCode(200)
  @ApiBearerAuth()
  @ApiResponse({ status: 200 })
  @ApiOperation({ summary: 'Sign Out user' })
  @UseGuards(AuthGuard(), RevokedTokenGuard)
  signOut(@Headers('authorization') authHeader: string): Promise<void>{
    const token = authHeader.split(' ')[1];
    return this.authService.signOut(token);
  }
}
