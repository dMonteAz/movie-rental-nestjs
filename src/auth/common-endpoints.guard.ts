import { Injectable, CanActivate, ExecutionContext } from "@nestjs/common";
import { AuthGuard, IAuthGuard } from '@nestjs/passport';
import { RevokedTokenGuard } from "./revoked-token.guard";

@Injectable()
export class CommonEndpointGuard implements CanActivate {
  constructor(
    private authGuard: any,
    private revokeGuard: RevokedTokenGuard,
  ){}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const headers = context.switchToHttp().getRequest().headers;
    if(!headers.authorization){
      return true;
    }

    return await this.authGuard.canActivate(context) && await this.revokeGuard.canActivate(context);
  }
}