import { EntityRepository, Repository } from "typeorm";
import { OauthToken } from "./oauth-token.entity";
import { JwtService } from '@nestjs/jwt';
import { User } from "src/users/user.entity";

@EntityRepository(OauthToken)
export class OauthTokenRepository extends Repository<OauthToken> {
  async createAccessToken(payload: object, jwtService: JwtService): Promise<OauthToken> {
    const accessToken = await jwtService.sign(payload, { secret: process.env.JWT_SECRET, expiresIn: (process.env.JWT_ACCESS_TOKEN_EXPIRATION || '1h') });
    const refreshToken = await jwtService.sign(payload, { secret: process.env.JWT_REFRESH_SECRET, expiresIn: (process.env.JWT_REFRESH_TOKEN_EXPIRATION || '24h') });
    const oauthToken = new OauthToken();
    oauthToken.token = accessToken;
    oauthToken.refreshToken = refreshToken;
    oauthToken.user = await User.findOne({id: payload['id']});
    await oauthToken.save();

    return oauthToken;
  }
}