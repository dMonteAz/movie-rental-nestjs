export enum MailTypeEnum {
  CONFIRMATION = 'confirmation',
  PASSWORD_CHANGE = 'password_change'
}