import { Injectable } from '@nestjs/common';
import { MailTypeEnum } from './mail-type.enum';
import { MailerService } from '@nestjs-modules/mailer';

@Injectable()
export class MailsService {
  constructor(
    private readonly mailerService: MailerService,
  ){}
  sendEmail(emails: string[], emailType: MailTypeEnum, options?: any){
    let details = null;
    switch(emailType) {
      case MailTypeEnum.CONFIRMATION: {
        details = this.confirmationEmailText(options);
        break;
      }
      default: {
        details = { text: 'Test Email', subject: 'Test Email' };
        break;
      }
    }
    this.mailerService.sendMail({
      to: emails, // list of receivers
      from: 'noreply@monte-movie-rental.com', // sender address
      subject: details.subject, // Subject line
      text: details.text, // plaintext body
    })
    .then(() => console.log('Email sended successfully'))
    .catch(()=> console.log('Failed to send email'));
  }

  private confirmationEmailText(options: any){
    const text = `Thank you for using Monte Movie Rental!\n 
Please use the following token to confirm your email\n
token: ${options['confirmationToken']}`;
    const subject = 'Confirmation Email';
    return { text, subject };
  }
}
