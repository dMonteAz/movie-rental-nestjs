import {MigrationInterface, QueryRunner} from "typeorm";

export class AddTypeFieldToOrderLine1595127845592 implements MigrationInterface {
    name = 'AddTypeFieldToOrderLine1595127845592'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "order_line" ADD "type" character varying NOT NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "order_line" DROP COLUMN "type"`);
    }

}
