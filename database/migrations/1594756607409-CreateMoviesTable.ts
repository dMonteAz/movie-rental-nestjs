import {MigrationInterface, QueryRunner} from "typeorm";

export class CreateMoviesTable1594756607409 implements MigrationInterface {
    name = 'CreateMoviesTable1594756607409'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "movie" ("id" SERIAL NOT NULL, "title" character varying NOT NULL, "stock" integer NOT NULL, "rentalPrice" integer NOT NULL, "salePrice" integer NOT NULL, "availability" boolean NOT NULL, CONSTRAINT "PK_cb3bb4d61cf764dc035cbedd422" PRIMARY KEY ("id"))`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE "movie"`);
    }

}
