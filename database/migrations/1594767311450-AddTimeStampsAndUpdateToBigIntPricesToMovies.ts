import {MigrationInterface, QueryRunner} from "typeorm";

export class AddTimeStampsAndUpdateToBigIntPricesToMovies1594767311450 implements MigrationInterface {
    name = 'AddTimeStampsAndUpdateToBigIntPricesToMovies1594767311450'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "movie" ADD "createdAt" TIMESTAMP NOT NULL DEFAULT now()`);
        await queryRunner.query(`ALTER TABLE "movie" ADD "updatedAt" TIMESTAMP NOT NULL DEFAULT now()`);
        await queryRunner.query(`ALTER TABLE "movie" DROP COLUMN "rentalPrice"`);
        await queryRunner.query(`ALTER TABLE "movie" ADD "rentalPrice" bigint NOT NULL`);
        await queryRunner.query(`ALTER TABLE "movie" DROP COLUMN "salePrice"`);
        await queryRunner.query(`ALTER TABLE "movie" ADD "salePrice" bigint NOT NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "movie" DROP COLUMN "salePrice"`);
        await queryRunner.query(`ALTER TABLE "movie" ADD "salePrice" integer NOT NULL`);
        await queryRunner.query(`ALTER TABLE "movie" DROP COLUMN "rentalPrice"`);
        await queryRunner.query(`ALTER TABLE "movie" ADD "rentalPrice" integer NOT NULL`);
        await queryRunner.query(`ALTER TABLE "movie" DROP COLUMN "updatedAt"`);
        await queryRunner.query(`ALTER TABLE "movie" DROP COLUMN "createdAt"`);
    }

}
