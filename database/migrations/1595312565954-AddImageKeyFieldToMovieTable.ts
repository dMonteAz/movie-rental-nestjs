import {MigrationInterface, QueryRunner} from "typeorm";

export class AddImageKeyFieldToMovieTable1595312565954 implements MigrationInterface {
    name = 'AddImageKeyFieldToMovieTable1595312565954'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "movie" ADD "imageKey" character varying NOT NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "movie" DROP COLUMN "imageKey"`);
    }

}
