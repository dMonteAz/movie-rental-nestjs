import {MigrationInterface, QueryRunner} from "typeorm";

export class UpdateSaltTypeToStringFromNumberOnUser1594936578420 implements MigrationInterface {
    name = 'UpdateSaltTypeToStringFromNumberOnUser1594936578420'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "salt"`);
        await queryRunner.query(`ALTER TABLE "user" ADD "salt" character varying NOT NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "salt"`);
        await queryRunner.query(`ALTER TABLE "user" ADD "salt" integer NOT NULL`);
    }

}
