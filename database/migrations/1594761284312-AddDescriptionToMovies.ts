import {MigrationInterface, QueryRunner} from "typeorm";

export class AddDescriptionToMovies1594761284312 implements MigrationInterface {
    name = 'AddDescriptionToMovies1594761284312'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "movie" ADD "description" character varying NOT NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "movie" DROP COLUMN "description"`);
    }

}
