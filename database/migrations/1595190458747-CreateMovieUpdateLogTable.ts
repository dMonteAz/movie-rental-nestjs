import {MigrationInterface, QueryRunner} from "typeorm";

export class CreateMovieUpdateLogTable1595190458747 implements MigrationInterface {
    name = 'CreateMovieUpdateLogTable1595190458747'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "movies_update_log" ("id" SERIAL NOT NULL, "oldValues" character varying NOT NULL, "newValues" character varying NOT NULL, "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), "userId" integer, "movieId" integer, CONSTRAINT "PK_b043054d9d7d30c7bdb5ffc4169" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "movies_update_log" ADD CONSTRAINT "FK_987fb91ac12d3c2ebb30a3bf653" FOREIGN KEY ("userId") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "movies_update_log" ADD CONSTRAINT "FK_130d2808567ae23d84f4517582a" FOREIGN KEY ("movieId") REFERENCES "movie"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "movies_update_log" DROP CONSTRAINT "FK_130d2808567ae23d84f4517582a"`);
        await queryRunner.query(`ALTER TABLE "movies_update_log" DROP CONSTRAINT "FK_987fb91ac12d3c2ebb30a3bf653"`);
        await queryRunner.query(`DROP TABLE "movies_update_log"`);
    }

}
