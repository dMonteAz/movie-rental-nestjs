import {MigrationInterface, QueryRunner} from "typeorm";

export class UpdatePriceFiledsTypeToBigInt1595115633219 implements MigrationInterface {
    name = 'UpdatePriceFiledsTypeToBigInt1595115633219'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "order" DROP COLUMN "transactionTotal"`);
        await queryRunner.query(`ALTER TABLE "order" ADD "transactionTotal" bigint NOT NULL DEFAULT 0`);
        await queryRunner.query(`ALTER TABLE "order_line" DROP COLUMN "price"`);
        await queryRunner.query(`ALTER TABLE "order_line" ADD "price" bigint NOT NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "order_line" DROP COLUMN "price"`);
        await queryRunner.query(`ALTER TABLE "order_line" ADD "price" integer NOT NULL`);
        await queryRunner.query(`ALTER TABLE "order" DROP COLUMN "transactionTotal"`);
        await queryRunner.query(`ALTER TABLE "order" ADD "transactionTotal" integer NOT NULL DEFAULT 0`);
    }

}
