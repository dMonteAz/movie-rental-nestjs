import {MigrationInterface, QueryRunner} from "typeorm";

export class CreateOrderLineTable1595115021653 implements MigrationInterface {
    name = 'CreateOrderLineTable1595115021653'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "order_line" ("id" SERIAL NOT NULL, "price" integer NOT NULL, "quantity" integer NOT NULL, "status" character varying NOT NULL, "rentedAt" TIMESTAMP, "daysRented" integer, "returnedAt" TIMESTAMP, "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), "orderId" integer, "movieId" integer, CONSTRAINT "PK_01a7c973d9f30479647e44f9892" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "order_line" ADD CONSTRAINT "FK_239cfca2a55b98b90b6bef2e44f" FOREIGN KEY ("orderId") REFERENCES "order"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "order_line" ADD CONSTRAINT "FK_573ae407e748bf1d060bf9ba53e" FOREIGN KEY ("movieId") REFERENCES "movie"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "order_line" DROP CONSTRAINT "FK_573ae407e748bf1d060bf9ba53e"`);
        await queryRunner.query(`ALTER TABLE "order_line" DROP CONSTRAINT "FK_239cfca2a55b98b90b6bef2e44f"`);
        await queryRunner.query(`DROP TABLE "order_line"`);
    }

}
